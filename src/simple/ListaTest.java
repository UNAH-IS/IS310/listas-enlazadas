/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simple;

import java.util.Scanner;

/**
 *
 * @author Jose Enrique Avila <jeavila@unah.edu.hn>
 */
public class ListaTest {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        menu();
    }
    
    // Esta funcion permite capturar un entero para diversas acciones
    static int solicitarNumero(Scanner input, String mensaje) {
        int dato;
        
        System.out.print(mensaje);
        
        try {
            dato = Integer.valueOf(input.nextLine());
        } catch(NumberFormatException ex) {
            dato = 0;
        }
        
        return dato;
    }

    static void menu() {
        int opcion, dato;
        Scanner input;
        Nodo nodo;
        String mensajeSolicitaDato = "Defina el dato (cero por defecto): ";
        Lista lista = new Lista();

        input = new Scanner(System.in);
        opcion = -1;
        
        while (opcion != 0) {
            System.out.println("\n\n.........................................");
            System.out.println("Operaciones de Listas Enlazadas Simples");
            System.out.println("---------------------------------------");
            System.out.println("1. Listar nodos");
            System.out.println("2. Agregar nodo (primero)");
            System.out.println("3. Agregar nodo (enmedio)");
            System.out.println("4. Agregar nodo (ultimo)");
            System.out.println("5. Eliminar nodo (primero)");
            System.out.println("6. Eliminar nodo (enmedio)");
            System.out.println("7. Eliminar nodo (ultimo)");
            System.out.println("8. Vaciar lista");
            System.out.println("0. Salir");
            System.out.print("......................................... ");
            
            opcion = solicitarNumero(input, "");
            
            switch(opcion) {
                case 1:
                    lista.recorrer();
                    break;
                case 2:
                    dato = solicitarNumero(input, mensajeSolicitaDato);
                    nodo = new Nodo(dato);
                    lista.insertarPrimero(nodo);
                    break;
                case 3:
                    dato = solicitarNumero(input, mensajeSolicitaDato);
                    nodo = new Nodo(dato);
                    
                    int datoAnterior = solicitarNumero(input, 
                            "Buscar dato del nodo anterior: ");
                    Nodo nodoAnterior = lista.encontrarPorDato(datoAnterior);
                    lista.insertarEnmedio(nodo, nodoAnterior);
                    break;
                case 4:
                    dato = solicitarNumero(input, mensajeSolicitaDato);
                    nodo = new Nodo(dato);
                    lista.insertarUltimo(nodo);
                    break;
                case 5:
                    lista.eliminarPrimero();
                    break;
                case 6:
                    dato = solicitarNumero(input, 
                            "Buscar dato del nodo a eliminar: ");
                    nodo = lista.encontrarPorDato(dato);
                    lista.eliminarEnmedio(nodo);
                    break;
                case 7:
                    lista.eliminarUltimo();
                    break;
                case 8:
                    lista.vaciar();
                    break;
                default:
            }
        }
    }

}
