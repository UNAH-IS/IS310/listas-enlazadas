/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simple;

/**
 *
 * @author Jose Enrique Avila <jeavila@unah.edu.hn>
 */
public class Lista {

    private Nodo primero;
    private Nodo ultimo;

    public Lista() {
        vaciar();
    }

    public void vaciar() {
        primero = null;
        ultimo = null;
    }

    public boolean estaVacia() {
        return primero == null && ultimo == null;
    }

    public void recorrer() {
        int contador;
        Nodo item;

        if (estaVacia()) {
            System.out.println("Lista vacia!");
        } else {
            System.out.println("Primero -> " + primero);
            System.out.println("Ultimo -> " + ultimo);
            
            contador = 0;
            item = primero;

            do {
                System.out.printf("Nodo%d -> %s %n", contador, item);
                item = item.getSiguiente();
                contador++;
            } while (item != null);
        }
    }

    public void insertarPrimero(Nodo n) {
        if (estaVacia()) {
            ultimo = n;
        } else {
            n.setSiguiente(primero);
        }

        primero = n;
    }

    public void insertarUltimo(Nodo n) {
        if (estaVacia()) {
            insertarPrimero(n);
        } else {
            ultimo.setSiguiente(n);
            ultimo = n;
        }
    }

    public void insertarEnmedio(Nodo n, Nodo nAnterior) {
        if (nAnterior == null) {
            return;
        }
        
        if (estaVacia()) {
            insertarPrimero(n);
        } else {
            if (ultimo == nAnterior) {
                ultimo = n;
            }
            
            n.setSiguiente(nAnterior.getSiguiente());            
            nAnterior.setSiguiente(n);
        }
    }

    public Nodo eliminarPrimero() {
        Nodo eliminado = null;        
        
        if (!estaVacia()) {            
            eliminado = primero;

            if (primero == ultimo) {
                vaciar();
            } else {
                primero = primero.getSiguiente();
                eliminado.setSiguiente(null);
            }
        }

        return eliminado;
    }

    public Nodo eliminarUltimo() {
        Nodo eliminado = null;
        
        if (!estaVacia()) {
        
            eliminado = ultimo;

            if (primero == ultimo) {
                vaciar();
            } else {
                Nodo item = primero;

                while(item.getSiguiente() != ultimo) {
                    item = item.getSiguiente();
                }

                ultimo = item;
                item.setSiguiente(null);
            }
        }

        return eliminado;
    }
        
    
    public Nodo eliminarEnmedio(Nodo n) {
        Nodo eliminado = null;
        
        if (n != null && !estaVacia()) {
            eliminado = n;
        
            if (n == primero) {
                eliminarPrimero();
            } else if (n == ultimo) {
                eliminarUltimo();
            } else {
                Nodo item = primero;

                while (item.getSiguiente() != n) {
                    item = item.getSiguiente();
                } 

                item.setSiguiente(item.getSiguiente().getSiguiente());
                n.setSiguiente(null);
            }
        }
        
        return eliminado;
    }
    
    public Nodo encontrarPorDato(int dato) {
        Nodo hallado = null;
        
        if (!estaVacia()) {
            Nodo item = primero;

            while (item != null) {
                // Una vez hallado el Nodo debe romperse el bucle
                if (item.getDato() == dato) {
                    hallado = item;
                    break;
                }

                item = item.getSiguiente();
            }
        }
        
        return hallado;
    }
    
    public Nodo encontrarPorIndice(int indice) {
        Nodo hallado = null;
        
        if (!estaVacia()) {
            Nodo item = primero;
            int conteo = 0;

            while (item != ultimo) {
                // Una vez hallado el Nodo debe romperse el bucle
                if (conteo == indice) {
                    hallado = item;
                    break;
                }

                conteo++;
                item = item.getSiguiente();
            }
        }
        
        return hallado;
    }
}
