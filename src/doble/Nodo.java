/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package doble;

/**
 *
 * @author Jose Enrique Avila <jeavila@unah.edu.hn>
 */
public class Nodo {
    private int dato;
    Nodo siguiente;
    Nodo anterior;
    
    public Nodo() {
        this.dato = 0;
        this.siguiente = null;
    }
    
    public Nodo(int dato) {
        this.dato = dato;
        this.siguiente = null;
    }
    
    public int getDato() {
        return this.dato;
    }
    
    @Override
    public String toString() {
        return String.format("Dato: %s", this.dato);
    }
}
