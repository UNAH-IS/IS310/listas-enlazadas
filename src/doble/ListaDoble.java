/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package doble;


/**
 *
 * @author Jose Enrique Avila <jeavila@unah.edu.hn>
 */
public class ListaDoble {

    private Nodo primero;
    private Nodo ultimo;
    private int conteo;    

    public ListaDoble() {
        vaciar();
    }

    public void vaciar() {
        primero = null;
        ultimo = null;
        conteo = 0;
    }

    public boolean estaVacia() {
        return primero == null && ultimo == null;
    }
    
    public void recorrer() {
        int contador;
        Nodo item;

        if (estaVacia()) {
            System.out.println("Lista vacia!");
        } else {
            System.out.println("Primero -> " + primero);
            System.out.println("Ultimo -> " + ultimo);
            
            contador = 0;
            item = primero;

            do {
                System.out.printf("Nodo%d -> %s %n", contador, item);
                item = item.siguiente;
                contador++;
            } while (item != null);
        }
    }
    
    public void recorrerReverso() {
        int contador;
        Nodo item;

        if (estaVacia()) {
            System.out.println("Lista vacia!");
        } else {
            System.out.println("Primero -> " + primero);
            System.out.println("Ultimo -> " + ultimo);
            
            contador = 0;
            item = ultimo;

            do {
                System.out.printf("Nodo%d -> %s %n", contador, item);
                item = item.anterior;
                contador++;
            } while (item != null);
        }
    }

    public void insertarPrimero(Nodo n) {
        if (estaVacia()) {
            ultimo = n;
        } else {
            n.siguiente = primero;
            primero.anterior = n;
        }

        primero = n;
    }

    public void insertarUltimo(Nodo n) {
        if (estaVacia()) {
            insertarPrimero(n);
        } else {
            ultimo.siguiente = n;
            n.anterior = ultimo;
            ultimo = n;
        }
    }
    
    public void insertarEnmedio(Nodo n, Nodo nAnterior) {
        if (nAnterior == null) {
            return;
        }
        
        if (estaVacia()) {
            insertarPrimero(n);
        } else {
            if (ultimo == nAnterior) {
                ultimo = n;
            } else {
                Nodo nSiguiente = nAnterior.siguiente;
                n.siguiente = nSiguiente;
                nSiguiente.anterior = n;
            }

            nAnterior.siguiente = n;
            n.anterior = nAnterior;
        }
    }

    public Nodo eliminarPrimero() {
        Nodo eliminado = null;        
        
        if (!estaVacia()) {            
            eliminado = primero;

            if (primero == ultimo) {
                vaciar();
            } else {
                primero = primero.siguiente;
                primero.anterior = null;
            }
        }

        return eliminado;
    }

    public Nodo eliminarUltimo() {
        Nodo eliminado = null;
        
        if (!estaVacia()) {        
            eliminado = ultimo;

            if (primero == ultimo) {
                vaciar();
            } else {
                Nodo nAnterior = ultimo.anterior;
                ultimo = ultimo.anterior;
                nAnterior.siguiente = null;
            }
        }
        
        return eliminado;
    }
        
    
    public Nodo eliminarEnmedio(Nodo n) {
        Nodo eliminado = null;
        
        if (n != null && !estaVacia()) {
            eliminado = n;
        
            if (n == primero) {
                eliminarPrimero();
            } else if (n == ultimo) {
                eliminarUltimo();
            } else {
                Nodo nSiguiente = n.siguiente;
                Nodo nAnterior = n.anterior;
                
                nSiguiente.anterior = nAnterior;
                nAnterior.siguiente = nSiguiente;
            }
        } else {
        }
        
        return eliminado;
    }
    
    public Nodo encontrarPorDato(int dato) {
        Nodo hallado = null;
        
        if (!estaVacia()) {
            Nodo item = primero;

            while (item != null) {
                // Una vez hallado el Nodo debe romperse el bucle
                if (item.getDato() == dato) {
                    hallado = item;
                    break;
                }

                item = item.siguiente;
            }
        }
        
        return hallado;
    }
    
    public Nodo encontrarPorIndice(int indice) {
        Nodo hallado = null;
        
        if (!estaVacia()) {
            Nodo item = primero;
            int contador = 0;

            while (item != ultimo) {
                // Una vez hallado el Nodo debe romperse el bucle
                if (contador == indice) {
                    hallado = item;
                    break;
                }

                contador++;
                item = item.siguiente;
            }
        }
        
        return hallado;
    }
}
